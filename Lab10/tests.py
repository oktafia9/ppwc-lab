from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve
from .views import subscribe
from .views import user_validate
from .models import RegisterSubscriber
from .forms import SubscriberForm

class Lab10UnitTest(TestCase):
	def test_lab_10_url_is_exist(self):
		response = Client().get('/lab-10/subscribe/')
		self.assertEqual(response.status_code,200)
		
	def test_lab_10_url_kedua_is_exist(self):
		response = Client().get('/lab-10/cekvalidasi/')
		self.assertEqual(response.status_code,200)
		
	def test_lab_10_using_template(self):
		response = Client().get('/lab-10/subscribe/')
		self.assertTemplateUsed(response, 'subscribe.html')
		
	def test_lab_10_using_subscribe_func(self):
		found = resolve('/lab-10/subscribe/')
		self.assertEqual(found.func, subscribe)
	
	def test_lab_10_using_user_validate_func(self):
		found = resolve('/lab-10/cekvalidasi/')
		self.assertEqual(found.func, user_validate)
		
	def test_model_can_create_new_register(self):
        # Creating a new activity
		new_activity = RegisterSubscriber.objects.create(nama=' ', email=' ', password=' ')

        # Retrieving all available activity
		counting_all_available_todo = RegisterSubscriber.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)
	
	def test_lab_10_can_save_a_POST_request(self):
		response = self.client.post('/lab-10/subscribe/', data = {
		'nama'   : ' ',
		'email'  : ' ',
		'password' : ' ',
		})
		counting_all_available_subscriber = RegisterSubscriber.objects.all().count()
		self.assertEqual(counting_all_available_subscriber, 1)