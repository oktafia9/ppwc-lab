from django.conf.urls import url
from .views import subscribe, user_validate, unsub 
from django.views.generic.base import RedirectView

app_name = "Lab10"
urlpatterns = [
	url('subscribe/', subscribe, name = 'subscribe'),
    url('cekvalidasi/', user_validate , name="cekvalidasi"),
	url('hapusSub/', unsub, name = 'unsub'),
]
