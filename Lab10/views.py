from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, HttpResponse
from .forms import SubscriberForm
from .models import RegisterSubscriber
import requests, json, re


# function POST request subscriber
@csrf_exempt
def subscribe(request):
    get_user_acccount = RegisterSubscriber.objects.all()
    if request.method == "POST":
        form_value = SubscriberForm(request.POST or None)
        nama = request.POST['nama']
        email = request.POST['email']
        password = request.POST['password']
        user_filter_email = RegisterSubscriber.objects.filter(email=email)
        if (len(user_filter_email) > 0):
            response_message = 'Use another email!'
        else:
            create_user = RegisterSubscriber(email=email, nama=nama, password=password)
            create_user.save()

    else:
        form_value = SubscriberForm()

    return render(request, 'subscribe.html', {'form': form_value})


@csrf_exempt
def user_validate(request):
    
    nama_validate_check = False
    email_validate_check = False
    password_validate_check = False
    
    if request.method == "POST":
        nama = request.POST['nama']
        email = request.POST['email']
        password = request.POST['password']

        # nama harus >= 4 dan <=50        
        if 4 <= len(nama) <= 50:
            nama_validate_check = True

        # email harus dicek
        if len(email) >= 8:
            email_validate_check = bool(re.match("^.+@(\[?)[a-zA-Z0-9-.]+.([a-zA-Z]{2,3}|[0-9]{1,3})(]?)$", email))

        # password harus lebih dr 8 dan kurang dari 50
        if 8 <= len(password) <= 50:
            password_validate_check = True

        # pesan kalo nama gasesuai
        if not nama_validate_check:
            return HttpResponse(json.dumps({'message': 'Name min 4 and max 30!'}), content_type="application/json")

        # pesan kalo email gasesuai
        if not email_validate_check:
            return HttpResponse(json.dumps({'message': 'Please enter the email correctly!'}), content_type="application/json")

        # pesan kalo email udh dipake
        user_filter_email = RegisterSubscriber.objects.filter(email=email)
        if len(user_filter_email) > 0:
            return HttpResponse(json.dumps({'message': 'Email already registered!'}), content_type="application/json")

        # kalo pas gasesuai
        if not password_validate_check:
            return HttpResponse(json.dumps({'message': 'Pass must contain at least 8 characters'}), content_type="application/json")

        return HttpResponse(json.dumps({'message': 'All fields are valid'}), content_type="application/json")

    else:
        return HttpResponse(json.dumps({'message': "Try again!"}), content_type="application/json")

def model_to_dict(obj):
	data = serializers.serialize('json',[obj,])
	struct = json.loads(data)
	data = json.dumps(struct[0]["fields"])
	return data

@csrf_exempt
def unsub(request):
	if request.method == "POST":
		subId = request.POST['id']
		RegisterSubscriber.objects.filter(id=subId).delete()
		return HttpResponse(json.dumps({'message': "Succeed"}),content_type="application/json")
	else:
		return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),content_type="application/json")
