from django.contrib import admin

# Register your models here.

from .models import RegisterSubscriber

admin.site.register(RegisterSubscriber)
