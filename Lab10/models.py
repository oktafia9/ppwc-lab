# Create your models here.
from django.db import models

# models of regist subscriber
class RegisterSubscriber(models.Model):
    nama = models.CharField(max_length=30)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=30)
