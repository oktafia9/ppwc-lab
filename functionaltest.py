import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class Lab6FunctionalTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.implicitly_wait(5)
        self.selenium.quit()
        super(Lab6FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://ppwc-oktafias.herokuapp.com/lab-6/')

        # find the form element
        status = selenium.find_element_by_name('description')

        # Fill the form with data
        status.send_keys('Coba Coba')

        # Set time to open web
        time.sleep(10)

        # Check if status successfully added
        self.assertIn("Coba Coba", self.selenium.page_source)
        
        # submitting the form
        status.submit()

    def test_input_position(self):
        selenium = self.selenium
        selenium.get('http://ppwc-oktafias.herokuapp.com/lab-6/')

        position1 = selenium.find_element_by_xpath("//body//center//h1").location
        self.assertEqual({'x':8, 'y':8}, position1)

        position2 = selenium.find_element_by_xpath("//body//div//a").location
        self.assertEqual({'x':8, 'y':84}, position2)

    def test_input_css(self):
        selenium = self.selenium
        selenium.get('http://ppwc-oktafias.herokuapp.com/lab-6/')

        css1 = selenium.find_element_by_xpath("//body//center//h1")
        self.assertIn("Hello, Apa Kabar?", css1.text)

        css2 = selenium.find_element_by_xpath("//body//div//a")
        self.assertIn("Lihat Profile Saya", css2.text)

    def test_button_clicked(self):
        selenium = self.selenium 
        selenium.get('http://ppwc-oktafias.herokuapp.com/lab-6/')
        button1 = selenium.find_element_by_id('but1')
        button2 = selenium.find_element_by_id('but2')
        button1.click()
        button2.click()

        
if __name__ == '__main__': #
    unittest.main(warnings='ignore')
