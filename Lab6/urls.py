from django.conf.urls import url
from .views import index, add_todo, show_identitas, list_buku, daftar_buku, login, logout
from . import views
from django.urls import include
    #url for app
app_name = "Lab6"
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_todo', add_todo, name='add_todo'),
    url(r'^identitas', show_identitas, name='identitas'),
	url(r'^listbuku', list_buku, name='listbuku'),
	url(r'^list_buku', daftar_buku, name='list_buku'),
	url('login/', login, name='login'),
    url('logout/', logout, name='logout'),
	
    

        
]
