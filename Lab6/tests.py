from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .views import show_identitas
from .views import daftar_buku
from .views import list_buku
from .models import Todo
from .forms import Todo_Form
from django.utils import timezone

class Lab6UnitTest(TestCase):
	def test_lab_6_url_is_exist(self):
		response = Client().get('/lab-6/')
		self.assertEqual(response.status_code,200)
    
	def test_lab_6_using_to_do_list_template(self):
		response = Client().get('/lab-6/')
		self.assertTemplateUsed(response,'to_do_list.html')

	def test_lab_6_profile_using_to_do_list_template(self):
		response = Client().get('/lab-6/identitas/')
		self.assertTemplateUsed(response, 'biodata.html')
	   
	def test_lab_6_list_buku_using_to_do_list_template(self):
		response = Client().get('/lab-6/listbuku/')
		self.assertTemplateUsed(response, 'list_buku.html')

	def test_lab_6_using_index_func(self):
		found = resolve('/lab-6/')
		self.assertEqual(found.func, index)

	def test_lab_6_profile_using_index_func(self):
		found = resolve('/lab-6/identitas/')
		self.assertEqual(found.func, show_identitas)
		
	def test_lab_6_list_buku_using_index_func(self):
		found = resolve('/lab-6/list_buku/')
		self.assertEqual(found.func, daftar_buku)

	def test_model_can_create_new_todo(self):
        # Creating a new activity
		new_activity = Todo.objects.create(description=' ')

        # Retrieving all available activity
		counting_all_available_todo = Todo.objects.all().count()
		self.assertEqual(counting_all_available_todo, 1)
        
	def test_form_todo_input_has_placeholder_and_css_classes(self):
		form = Todo_Form()
		self.assertIn('class="todo-form-textarea', form.as_p())
		self.assertIn('id="id_description', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = Todo_Form(data={'description': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
		form.errors['description'],["This field is required."]
		)

	def test_lab6_post_success_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/lab-6/add_todo', {'description': test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/lab-6/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_lab6_post_error_and_render_the_result(self):
		test = 'Anonymous'
		response_post = Client().post('/lab-6/add_todo', {'description': ''} )
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/lab-6/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

        



