from django.contrib import admin

# Register your models here.

from .models import Todo

class AuthorAdmin(admin.ModelAdmin):
    admin.site.register(Todo)