from django import forms

from django.forms import Form
from .models import Todo


class Todo_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
    
    description_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'class': 'todo-form-textarea',
        'placeholder':'Masukan status...'
    }
    description = forms.CharField(label='', required=True, max_length=300, widget=forms.Textarea(attrs=description_attrs))
    time = forms.DateTimeInput(attrs={'class': 'form-control'})

class Meta :
    model = Todo
    fields = ['description']

