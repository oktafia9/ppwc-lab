from django.urls import reverse
from .forms import Todo_Form
from .models import Todo

from django.http import HttpResponseRedirect
from django.shortcuts import render

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from django.shortcuts import render, render_to_response
from django.template import RequestContext, loader
import datetime
import json
import requests
from django.contrib.auth import logout as logout_user

    # Create your views here.

response = {}
def index(request):
    response['author'] = "Oktafia" #TODO Implement yourname
    todo = Todo.objects.all()
    response['todo'] = todo
    response['todo_form'] = Todo_Form
    return render(request, 'to_do_list.html', response)

def add_todo(request):
    form = Todo_Form(request.POST or None)
    if(request.method == 'POST'):
        response['description'] = request.POST['description']
        todo = Todo(description=response['description'])
        todo.save(todo)
        return HttpResponseRedirect('/lab-6/')
    else:
        return HttpResponseRedirect('/lab-6/')

def show_identitas(request) :
    return render(request, 'biodata.html')


def list_buku(request) :
	return render(request, 'list_buku.html')

def daftar_buku(request) :
	data = requests.get('https://www.googleapis.com/books/v1/volumes?q=quilting')
	return HttpResponse(data, content_type='application/json')
	
def login(request):
    return render(request, 'login.html')

def logout(request):
    logout_user(request)
    return redirect('to_do_list.html')
