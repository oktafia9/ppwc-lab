from django import forms
from django.db import models
from django.utils import timezone
from datetime import datetime, date


# Create your models here.
class Todo(models.Model):
    description = models.TextField()
    time = models.DateTimeField(auto_now_add = True)

